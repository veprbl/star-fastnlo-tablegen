# --- Use emacs in sh mode -*-sh-*- #
# This is a automatically generated file by fastNLO and holds the values of the warmup run. 
# The values are valid for the scenario fnlXXXX_fix
# and if calculated with the steerfile: InclusiveJets.str
# but only if no serious changes have been performed since its creation.
# 
# Delete this file, if you want fastNLO to calculate a new one.
# 
# This file has been calculated using 1e+09 contributions.
#   ( Mind: contributions != events. And contributions are not necessarily in phase space region.
# Please check by eye for reasonability of the values.
 
Warmup.OrderInAlphasOfWarmupRunWas	2
Warmup.CheckScaleLimitsAgainstBins	true
Warmup.ScaleDescriptionScale1     	"pT_jet_[GeV]"
Warmup.DifferentialDimension      	1
Warmup.DimensionLabels {
  "pT_[GeV]"  
} 
Warmup.DimensionIsDifferential {
  "2"  
} 

Warmup.Values {{
   ObsBin       x_min      x_max  pT_jet_[GeV]_min  pT_jet_[GeV]_max
      0      2.66e-02   1.00e+00             12.69             14.08
      1      3.01e-02   1.00e+00             14.08             15.61
      2      3.40e-02   1.00e+00             15.61             17.31
      3      3.86e-02   1.00e+00             17.31             19.20
      4      4.40e-02   1.00e+00             19.20             21.30
      5      5.03e-02   1.00e+00             21.30             23.62
      6      5.78e-02   1.00e+00             23.62             26.19
      7      6.66e-02   1.00e+00             26.19             29.05
      8      7.73e-02   1.00e+00             29.05             32.22
      9      9.05e-02   1.00e+00             32.22             35.73
     10      1.07e-01   1.00e+00             35.73             39.63
     11      1.28e-01   1.00e+00             39.63             43.95
     12      1.55e-01   1.00e+00             43.95             48.74
     13      1.90e-01   1.00e+00             48.74             54.06
     14      2.34e-01   1.00e+00             54.06             59.96
     15      2.88e-01   1.00e+00             59.96             66.49
}}


Warmup.Binning {{
    ObsBin   pT_[GeV]_Lo   pT_[GeV]_Up       BinSize
       0         12.690        14.080         2.780
       1         14.080        15.610         3.060
       2         15.610        17.310         3.400
       3         17.310        19.200         3.780
       4         19.200        21.300         4.200
       5         21.300        23.620         4.640
       6         23.620        26.190         5.140
       7         26.190        29.050         5.720
       8         29.050        32.220         6.340
       9         32.220        35.730         7.020
      10         35.730        39.630         7.800
      11         39.630        43.950         8.640
      12         43.950        48.740         9.580
      13         48.740        54.060        10.640
      14         54.060        59.960        11.800
      15         59.960        66.490        13.060
}}
