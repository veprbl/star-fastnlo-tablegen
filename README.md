Scenario definition
-------------------

> The elementary pieces in fastNLO are called "scenarios". One scenario comprises
> the code to generate the interpolation table for an observable with input from
> external programs like NLOJet++, the resulting table itself, and the code to
> evaluate it under various assumptions on PDFs, scales, etc.

Software list
-------------

This repository contains following software packages:

* **fastjet** - jet reconstruction algorithms library used by calculation code
* **fastnlo\_toolkit** - collection of tools for merging tables and convoluting them with PDF's to produce cross sections
* **fastnlo\_interface\_nlojet** - nlojet++ user module that calculates FastNLO v2 tables
* **lhapdf** - interface to PDF database
* **nlojet++** - NLO calculation code
* **qcdnum** - PDF evolution code

Additional pieces
-----------------

To simplify the process of table production the repository provides following scripts:

* **install.sh** - compile and install all of the software into a current working directory (expected to be repo root)
* **genjob.py** - generate htcondor job files to run nlojet++ instances

Producing the table
-------------------

    ./install.sh                             # compile the software
    ./genjob > myjob.condor                  # generate a job file
    condor_submit myjob.condor               # submit job to the cluster
    bin/fnlo-tk-merge output/* fnlXXXX.tab   # merge results
    bin/fnlo-cppread fnlXXXX.tab             # convolve
