#!/usr/bin/env python

#
# The purpose of this utility is to read FastNLO warmup files
# and plot them on kinematic surface.
#

import array

import numpy as np
import ROOT

def mk_array(ar):
	if isinstance(ar, np.ndarray):
		ar = list(ar)
	return array.array("d", ar)

def plot_phase_space(input_file, color, fill_style):
	PS_START = "Warmup.Values {{"
	PS_END = "}}"
	fp = open(input_file, "r")
	lines = map(lambda s: s.strip(), fp.readlines())
	fp.close()
	start = lines.index(PS_START)+2
	end = lines.index(PS_END)
	rows = map(lambda l: map(lambda s: float(s),l.split()), lines[start:end])
	gs = []
	for i, x_min, x_max, pt_min, pt_max in rows:
		b = ROOT.TBox(x_min, pt_min**2, x_max, pt_max**2)
		b.SetFillColor(color)
		b.SetFillStyle(fill_style)
		b.Draw("SAME")
		gs.append(b)
	return gs

ROOT.gROOT.SetBatch(True)

xbins = np.logspace(-7, 0, 8)
ybins = np.logspace(-1.45, 8.68, 10)

ROOT.gStyle.SetPadTopMargin(0.004);
ROOT.gStyle.SetPadRightMargin(0.015);
ROOT.gStyle.SetPadBottomMargin(0.065);
ROOT.gStyle.SetPadLeftMargin(0.107);
c = ROOT.TCanvas("c", "", 827, 808)
c.SetFillStyle(4000);
c.SetFrameFillStyle(0);
c.SetLogx()
c.SetLogy()
h = ROOT.TH2F("h", "", len(xbins)-1, mk_array(xbins), len(ybins)-1, mk_array(ybins))
h.SetXTitle("x")
h.GetYaxis().SetTitleOffset(1.5)
h.SetYTitle("Q^{2}/GeV^{2}")
h.SetFillStyle(4000);
h.SetStats(False)
h.GetYaxis().SetLabelSize(0.04)
ROOT.TGaxis.SetExponentOffset(0.1, 0.5)
h.Draw()

gs1 = plot_phase_space("InclusiveJets_fnlSTAR2013incl_fix_warmup.txt", ROOT.kCyan, 3244)
gs2 = plot_phase_space("InclusiveJets_fnlXXXX_fix_warmup.txt", ROOT.kMagenta-4, 3244)

t = ROOT.TText(0.05, 500, "RHIC")
t.SetTextSize(0.06)
t.Draw()

legend = ROOT.TLegend(0.33, 0.65, 0.57, 0.77)
legend.AddEntry(gs1[0], "STAR 510 GeV", "F")
legend.AddEntry(gs2[0], "STAR 200 GeV", "F")
legend.SetTextColor(ROOT.kBlue)
legend.SetTextSize(0.03)
legend.SetFillStyle(4000)
legend.SetLineColor(ROOT.kWhite)
legend.Draw()
c.SaveAs("plot.eps")
