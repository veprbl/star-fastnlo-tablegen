#!/usr/bin/env python

import argparse
import random
import glob

parser = argparse.ArgumentParser()
parser.add_argument("--event_count", type=str, default="10G",
                    help="total event count to produce")
parser.add_argument("--num_jobs", type=int, default=1000,
                    help="count of condor jobs for each calculation order (LO, NLO)")
parser.add_argument("--seed", type=int, default=None,
                    help="random seed")
args = parser.parse_args()

if args.event_count[-1] == "G":
	# billions
	args.event_count = int(args.event_count[:-1]) * 1000000000
else:
	args.event_count = int(args.event_count)

if args.seed is not None:
	random.seed(args.seed)

events_per_job = args.event_count / args.num_jobs

warmup_files = glob.glob("InclusiveJets_*_warmup.txt")

print """Universe = vanilla
Executable = bin/nlojet++
Log = job.log
transfer_input_files = bin,lib,libexec,share,InclusiveJets.str,%s
should_transfer_files = YES
when_to_transfer_output = ON_EXIT
transfer_output_files = output
Notification = Never
+Experiment = "star"

""" % (",".join(warmup_files),)

for approx in ["born", "nlo"]:
    for i in range(args.num_jobs):
       task_name = "j%03i" % i
       print """

Arguments = --calculate -c%s -n%s -s%i -u lib/fastnlo_interface_nlojet/libInclusiveJets.la --max-event %i
Output = %s_%s.out
Queue""" % (approx, task_name, random.randint(0, 4294967296), events_per_job, approx, task_name)
