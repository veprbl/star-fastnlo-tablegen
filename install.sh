#!/bin/bash

set -xe

TMP=/tmp/star-fastnlo-tablegen-$$
BASEDIR="$(realpath "$(dirname "$0")")"
: ${JOB_COUNT:=-j$(($(nproc --all)/2))}
: ${INSTALLDIR:=$(pwd)/workdir}

rm -rf "$TMP"
mkdir "$TMP"
pushd "$TMP"

for i in "$BASEDIR"/tarballs/*.tar.gz; do
  tar zxf $i
done
pushd nlojet++-4.1.3
patch -p1 < "$BASEDIR"/nlojet_template_fix.patch || exit 1
find . -type f -regex '.+\.\(cc\|h\)' -print0 | xargs -0 sed -i -e 's/isinf/std::isinf/g' -e 's/isnan/std::isnan/g'
./configure --prefix=${INSTALLDIR} || exit 1
make clean
make install ${JOB_COUNT} || exit 1
popd
pushd fastjet-*
./configure --prefix=${INSTALLDIR} --enable-d0runiicone || exit 1
make clean
make install ${JOB_COUNT} || exit 1
popd
pushd LHAPDF-*
./configure --prefix=${INSTALLDIR} || exit 1
make clean
make install ${JOB_COUNT} || exit 1
pushd examples
make
install .libs/hessian2replicas ${INSTALLDIR}/bin
popd
popd
pushd fastnlo_toolkit-2.3.1pre-*
sed -i -e '1a#include <string.h>' fastnlotoolkit/fastNLOQCDNUMAS.cc
autoreconf -fvi || exit 1
./configure --prefix=${INSTALLDIR} --with-lhapdf=${INSTALLDIR} || exit 1
make clean
make install ${JOB_COUNT} || exit 1
popd
pushd fastnlo_interface_nlojet-2.3.1pre-*
autoreconf -fvi || exit 1
./configure --prefix=${INSTALLDIR} --with-nlojet=${INSTALLDIR} --with-fnlotoolkit=${INSTALLDIR} --with-fastjet=${INSTALLDIR} || exit 1
make clean
make install ${JOB_COUNT} || exit 1
popd
