#!/usr/bin/env python

"""
 Usage:
   (
     bin/fnlo-tk-cppread fnlXXXX.tab CT10nlo_rand17156 7
     bin/fnlo-tk-cppread fnlXXXX.tab CT10nlo_rand17156 1 1000
   ) > star2006_200_CT10nlo

   (
     bin/fnlo-tk-cppread fnlXXXX.tab HERAPDF15NLO_EIG_rand5560 7
     bin/fnlo-tk-cppread fnlXXXX.tab HERAPDF15NLO_EIG_rand5560 1 1000
   ) > star2006_200_HERAPDF15NLO_EIG

   (
     bin/fnlo-tk-cppread fnlXXXX.tab NNPDF30_nnlo_as_0118 7
     bin/fnlo-tk-cppread fnlXXXX.tab NNPDF30_nnlo_as_0118 1 100
   ) > star2006_200_NNPDF30_nnlo_as_0118


   (
     bin/fnlo-tk-cppread fnlSTAR2009_500incl.tab CT10nlo_rand17156 7
     bin/fnlo-tk-cppread fnlSTAR2009_500incl.tab CT10nlo_rand17156 1 1000
   ) > star2009_500_CT10nlo

   (
     bin/fnlo-tk-cppread fnlSTAR2009_500incl.tab HERAPDF15NLO_EIG_rand5560 7
     bin/fnlo-tk-cppread fnlSTAR2009_500incl.tab HERAPDF15NLO_EIG_rand5560 1 1000
   ) > star2009_500_HERAPDF15NLO_EIG

   (
     bin/fnlo-tk-cppread fnlSTAR2009_500incl.tab NNPDF30_nnlo_as_0118 7
     bin/fnlo-tk-cppread fnlSTAR2009_500incl.tab NNPDF30_nnlo_as_0118 1 100
   ) > star2009_500_NNPDF30_nnlo_as_0118


   ./cppread_plot.py --scenario star2006_200 --pdf_name CT10nlo --num_replicas 1000
   ./cppread_plot.py --scenario star2006_200 --pdf_name HERAPDF15NLO_EIG --num_replicas 1000
   ./cppread_plot.py --scenario star2006_200 --pdf_name NNPDF30_nnlo_as_0118 --num_replicas 100

   ./cppread_plot.py --scenario star2009_500 --pdf_name CT10nlo --num_replicas 1000
   ./cppread_plot.py --scenario star2009_500 --pdf_name HERAPDF15NLO_EIG --num_replicas 1000
   ./cppread_plot.py --scenario star2009_500 --pdf_name NNPDF30_nnlo_as_0118 --num_replicas 100
"""

import array
import itertools
from math import sqrt

import numpy as np
import ROOT

TABLE_HEADER_START = "  IObs  Bin Size IODimO"
TABLE_HEADER_EXPECTED = "  IObs  Bin Size IODimO  [ pT_[GeV]          ]  <mu1_[GeV]   >  LO cross section   NLO cross section   KNLO"
SCALE_FACT_LINE = " The scale factors xmur, xmuf chosen here are:"
MEMBER_LINE = "Using LHAPDF member "
LHAPDF6_PDFSET_LINE_TAG = "LHAPDF ID"

COL_ID = 0
COL_PT_MIN = 3
COL_PT_MAX = 4
COL_LO = 6
COL_NLO = 7

FASTNLO_SCALES_COUNT = 7

def mk_array(ar):
	if isinstance(ar, np.ndarray):
		ar = list(ar)
	return array.array("d", ar)


def take(n, it):
	"""
	Take n first elements from the iterator.
	"""
	retval = list(itertools.islice(it, 0, n))
	if len(retval) != n:
		raise ValueError("Iterator depleted")
	return retval


class Xsec(object):
	def __init__(self):
		self.name = ""
		self.id = []
		self.pt_min = []
		self.pt_max = []
		self.lo = []
		self.nlo = []
		self.value = []
		self.uncertainty_plus = []
		self.uncertainty_minus = []
		self.xmur = None
		self.xmuf = None
		self.member = None

	def __str__(self):
		rows = zip(self.id, self.pt_min, self.pt_max, self.lo, self.nlo)
		lines = map(lambda row: "\t".join(map(str, row)), rows)
		return "\n".join(lines)

	@property
	def pt_bins(self):
		if len(self.pt_min) == 0:
			assert len(self.pt_max) == 0
			return []
		for bin_min, bin_max_prev in zip(self.pt_min[1:], self.pt_max[:-1]):
			assert bin_min == bin_max_prev
		return self.pt_min + [self.pt_max[-1]]

	@property
	def bin_count(self):
		return len(self.pt_min)


def read_cppread_output(fp):
	"""
	Parses text output of fnlo-tk-cpp tool into Xsec objects
	"""
	xsec = None
	while 1:
		try:
			line = fp.next().rstrip()
		except StopIteration:
			# Bumped into EOF, return last xsec if possible
			if xsec is not None:
				yield xsec
			raise StopIteration
		if line.startswith(TABLE_HEADER_START):
			# Start of the new cross section table
			if xsec is not None:
				yield xsec
			xsec = Xsec()
			try:
				assert fp.next().startswith("------")
			except StopIteration:
				raise IOError("Unexpected EOF")
		elif line.find(LHAPDF6_PDFSET_LINE_TAG) != -1:
			pdf_name = line[:line.find(" PDF set")]
			# remove _randXXXX postfix added by hessian2replicas
			pos = pdf_name.find("_rand")
			if pos != -1:
				pdf_name = pdf_name[:pos]
		elif line.startswith(SCALE_FACT_LINE):
			# Read renormalization and factorization scale relative values
			csv_line = line[len(SCALE_FACT_LINE):]
			xmur, xmuf = map(lambda s: float(s.strip()), csv_line.split(","))
		elif line.startswith(MEMBER_LINE):
			# Read PDF set member id
			member = int(line[len(MEMBER_LINE):])
		else:
			# Read cross section table
			try:
				vals = map(float, line.split())
			except ValueError:
				if xsec is not None: 
					xsec.name = "NLO pQCD %s" % pdf_name
					xsec.xmur = xmur
					xsec.xmuf = xmuf
					xsec.member = member
					yield xsec
					del xmur, xmuf
					xsec = None
				continue
			if len(vals) > 0:
				assert len(vals) == 9
				xsec.id.append(vals[COL_ID])
				xsec.pt_min.append(vals[COL_PT_MIN])
				xsec.pt_max.append(vals[COL_PT_MAX])
				xsec.lo.append(vals[COL_LO])
				xsec.nlo.append(vals[COL_NLO])


def get_xsec_value(xsec, i):
	assert isinstance(xsec, Xsec)
	return xsec.nlo[i]


def mk_xsec_hist(name, title, pt_bins, xsec_bins, main=False):
	h = ROOT.TH2F(name, title, len(pt_bins)-1, mk_array(pt_bins), len(xsec_bins)-1, mk_array(xsec_bins))
	h.SetStats(False)
	h.GetZaxis().SetNdivisions(10, False);
	if main:
		h.SetXTitle("p_T, GeV");
		h.SetYTitle("xsec, pb/GeV");
		h.GetXaxis().SetTitleOffset(1.3)
		h.GetYaxis().SetTitleOffset(1.4)
	return h


def mk_xsec_distribution_plot(xsec_bins, xsecs):
	"""
	Plot distribution of cross sections in a set
	"""
	xsec_any = xsecs[0]
	pt_bins = xsec_any.pt_bins
	graphic_objects = []
	once = True
	for i, (pt_min, pt_max) in enumerate(zip(xsec_any.pt_min, xsec_any.pt_max)):
		h_probe = ROOT.TH1F("h_probe", "", len(xsec_bins)-1, mk_array(xsec_bins))
		for xsec in xsecs:
			h_probe.Fill(get_xsec_value(xsec, i))
		bin_max_xsec = h_probe.GetMaximum()
		if bin_max_xsec == 0:
			print "Warning: no cross sections in pt_bin [%.1f,%.1f]" % (pt_min, pt_max)
			continue
		del h_probe
		h_all = mk_xsec_hist("h_all%i" % i, "", pt_bins, xsec_bins)
		for xsec in xsecs:
			h_all.Fill(pt_min, get_xsec_value(xsec, i), 1.0 / bin_max_xsec)
		h_all.Draw("Z SAME COL" if once else "SAME COL")
		graphic_objects.append(h_all)
		once = False
	return graphic_objects


def mk_xsec_bin_by_bin_distribution_plots(c, pdf_name, xsecs):
	xsec_any = xsecs[0]
	graphic_objects = []
	for i, (pt_min, pt_max) in enumerate(zip(xsec_any.pt_min, xsec_any.pt_max)):
		title = "%s, %.1f - %.1f GeV/c" % (pdf_name, pt_min, pt_max)
		h = ROOT.TH1F("h_bin%i_distribution" % i, title, 20, 0, 0)
		h.SetXTitle("xsec, pb/GeV");
		for xsec in xsecs:
			h.Fill(get_xsec_value(xsec, i))
		c.cd(i+1)
		h.Draw()
		graphic_objects.append(h)
	return graphic_objects


def plot_rel_uncertainty(xsec, color):
	pt_bins = xsec.pt_bins
	h_p = ROOT.TH1F("h_p", "", len(pt_bins)-1, mk_array(pt_bins))
	h_n = ROOT.TH1F("h_n", "", len(pt_bins)-1, mk_array(pt_bins))
	h_p.SetStats(False)
	h_n.SetStats(False)
	h_p.SetFillColor(color)
	h_n.SetFillColor(color)
	table = zip(xsec.pt_min, xsec.value, xsec.uncertainty_plus, xsec.uncertainty_minus)
	for pt_min, xsec, uncertainty_plus, uncertainty_minus in table:
		h_p.SetBinContent(h_p.FindBin(pt_min), uncertainty_plus / xsec)
		h_n.SetBinContent(h_p.FindBin(pt_min), -uncertainty_minus / xsec)
	h_p.Draw("SAME")
	h_n.Draw("SAME")
	return (h_p, h_n)


def mk_xsec_exp_kfactor_plot(xsec_pdf_uncert, xsec_scale_uncert, xsec_exp):
	graphic_objects = []
	pt_bins = xsec_pdf_uncert.pt_bins
	h = ROOT.TH1F("h", "", len(pt_bins)-1, mk_array(pt_bins))
	#h.SetXTitle("p_T, GeV");
	h.SetYTitle("(exp - th)/th");
	h.GetXaxis().SetTitleOffset(0.5)
	h.GetXaxis().SetTitleSize(0.1)
	h.GetYaxis().SetTitleOffset(0.5)
	h.GetYaxis().SetTitleSize(0.1)
	h.SetStats(False)
	h.GetXaxis().SetLabelSize(0.1)
	h.GetYaxis().SetLabelSize(0.1)
	h.GetYaxis().SetRangeUser(-1, 1)
	h.GetYaxis().SetNdivisions(11)
	h.Draw()
	graphic_objects.append(h)
	legend = ROOT.TLegend(0.12, 0.12, 0.7, 0.35)
	legend.SetTextSize(0.1)
	legend.SetLineColor(ROOT.kWhite)
	graphic_objects.append(plot_rel_uncertainty(xsec_pdf_uncert, ROOT.kYellow))
	legend.AddEntry(graphic_objects[-1][0], "PDF uncertainty", "F")
	graphic_objects.append(plot_rel_uncertainty(xsec_scale_uncert, ROOT.TColor.GetColorTransparent(ROOT.kBlue, 0.5)))
	legend.AddEntry(graphic_objects[-1][0], "scale uncertainty", "F")
	legend.Draw()
	graphic_objects.append(legend)

	if xsec_exp is not None:
		xsec_exp_rel = Xsec()
		xsec_exp_rel.pt_min = xsec_exp.pt_min
		xsec_exp_rel.pt_max = xsec_exp.pt_max
		assert xsec_pdf_uncert.pt_bins == xsec_exp.pt_bins
		for xsec, uncertainty_plus, uncertainty_minus, xsec_th in zip(xsec_exp.value, xsec_exp.uncertainty_plus, xsec_exp.uncertainty_minus, xsec_pdf_uncert.value):
			xsec_exp_rel.value.append(xsec / xsec_th - 1)
			xsec_exp_rel.uncertainty_plus.append(uncertainty_plus / xsec_th)
			xsec_exp_rel.uncertainty_minus.append(uncertainty_minus / xsec_th)
		graphic_objects.append(mk_xsec_plot(xsec_exp_rel))
	return graphic_objects


def mk_xsec_plot(xsec):
	"""
	Plot a single cross section
	"""
	vx = (np.array(xsec.pt_max) + np.array(xsec.pt_min)) / 2
	vexl = vx - xsec.pt_min
	vexh = xsec.pt_max - vx
	vy = xsec.value
	veyl = np.array(xsec.uncertainty_minus)
	veyh = np.array(xsec.uncertainty_plus)
	g = ROOT.TGraphAsymmErrors(len(vy),
		mk_array(vx), mk_array(vy),
		mk_array(vexl), mk_array(vexh),
		mk_array(veyl), mk_array(veyh))
	g.Draw("SAME P")
	return g


def mk_xsec_bin_by_bin_plots(c, xsec):
	graphic_objects = []
	for i, (v, vep, vem) in enumerate(zip(xsec.value, xsec.uncertainty_plus, xsec.uncertainty_minus)):
		c.cd(i+1)
		l_central = ROOT.TLine(v, 0, v, 150)
		l_central.SetLineColor(ROOT.kRed)
		l_central.Draw()
		graphic_objects.append(l_central)
		l_plus = ROOT.TLine(v + vep, 0, v + vep, 100)
		l_plus.SetLineColor(ROOT.kMagenta)
		l_plus.Draw()
		graphic_objects.append(l_plus)
		l_minus = ROOT.TLine(v - vem, 0, v - vem, 100)
		l_minus.SetLineColor(ROOT.kMagenta)
		l_minus.Draw()
		graphic_objects.append(l_minus)
	return graphic_objects


def star_2006_inclusive_xsec():
	"""
	Return cross section values from Tai Sakuma's thesis
	"""
	xsec_exp = [
	[12.69, 14.08, 1.19, 0.11, 5, 0.41, 0.30,  1.572, 0.027, +0.507, -0.332],
	[14.08, 15.61, 5.18, 0.26, 4, 1.89, 1.48,  1.352, 0.022, +0.306, -0.255],
	[15.61, 17.31, 2.12, 0.08, 4, 0.77, 0.57,  1.156, 0.014, +0.323, -0.126],
	[17.31, 19.2,  9.82, 0.38, 3, 3.54, 2.75,  1.099, 0.016, +0.157, -0.124],
	[19.2,  21.3,  4.31, 0.16, 3, 1.53, 1.17,  1.056, 0.014, +0.110, -0.112],
	[21.3,  23.62, 1.81, 0.06, 3, 0.62, 0.48,  0.991, 0.012, +0.106, -0.078],
	[23.62, 26.19, 7.92, 0.27, 2, 2.68, 2.22,  0.974, 0.010, +0.073, -0.078],
	[26.19, 29.05, 3.34, 0.12, 2, 1.07, 0.88,  0.949, 0.010, +0.068, -0.053],
	[29.05, 32.22, 1.47, 0.06, 2, 0.46, 0.40,  0.941, 0.010, +0.057, -0.047],
	[32.22, 35.73, 5.33, 0.26, 1, 1.72, 1.48,  0.931, 0.011, +0.049, -0.051],
	[35.73, 39.63, 1.91, 0.09, 1, 0.69, 0.58,  0.922, 0.015, +0.048, -0.033],
	[39.63, 43.95, 6.29, 0.38, 0, 2.34, 1.75,  0.924, 0.020, +0.019, -0.060],
	[43.95, 48.74, 1.91, 0.11, 0, 0.79, 0.79,  0.920, 0.007, +0.040, -0.038],
	[48.74, 54.06, 4.44, 0.42, -1, 1.67, 1.35, 0.895, 0.010, +0.055, -0.046],
	[54.06, 59.96, 7.97, 1.29, -2, 5.78, 3.11, 0.888, 0.008, +0.045, -0.027],
	[59.96, 66.49, 1.15, 0.30, -2, 0.46, 0.6,  0.844, 0.007, +0.050, -0.060]
	]
	COL_EXP_PT_MIN = 0
	COL_EXP_PT_MAX = 1
	COL_EXP_XSEC = 2
	COL_EXP_STAT_ERR = 3
	COL_EXP_MANTISSA = 4
	COL_EXP_SYST_ERR_POS = 5
	COL_EXP_SYST_ERR_NEG = 6
	# Hadronization and UE corrections are included but not used
	COL_EXP_C = 7 # C = \sigma_hadron / \sigma_parton
	COL_EXP_C_STAT_ERR = 8
	COL_EXP_C_SYST_ERR_POS = 9
	COL_EXP_C_SYST_ERR_NEG = 10
	xsec = Xsec()
	xsec.name = "STAR Run-6, particle level"
	for row in xsec_exp:
		order_of_magnitude = 10 ** row[COL_EXP_MANTISSA]
		value = row[COL_EXP_XSEC] * order_of_magnitude
		uncertainty_plus = (row[COL_EXP_STAT_ERR] + row[COL_EXP_SYST_ERR_POS]) * order_of_magnitude
		uncertainty_minus = (row[COL_EXP_STAT_ERR] + row[COL_EXP_SYST_ERR_NEG]) * order_of_magnitude
		xsec.pt_min.append(row[COL_EXP_PT_MIN])
		xsec.pt_max.append(row[COL_EXP_PT_MAX])
		xsec.value.append(value)
		xsec.uncertainty_plus.append(uncertainty_plus)
		xsec.uncertainty_minus.append(uncertainty_minus)
	return xsec


def star_2009_500_inclusive_xsec():
	# PRELIMINARY
	xsec_exp = [
	[10  , 11.8, 3.4597     , 0.019925],
	[11.8, 13.9, 2.11814    , 0.00781641],
	[13.9, 16.4, 0.826437   , 0.00163222],
	[16.4, 19.4, 0.213317   , 0.000471094],
	[19.4, 22.9, 0.0631708  , 0.000154576],
	[22.9, 27  , 0.0249447  , 6.00714e-05],
	[27  , 31.9, 0.00905359 , 2.44187e-05],
	[31.9, 37.6, 0.00300418 , 1.00836e-05],
	[37.6, 44.4, 0.00103613 , 4.39362e-06],
	[44.4, 52.3, 0.000370009, 1.8588e-06],
	[52.3, 61.8, 0.000131079, 1.01112e-06],
	[61.8, 72.9, 4.11806e-05, 4.49818e-07],
	[72.9, 86  , 1.14493e-05, 1.45744e-07]
	]
	# PRELIMINARY
	COL_EXP_PT_MIN = 0
	COL_EXP_PT_MAX = 1
	COL_EXP_XSEC = 2
	COL_EXP_STAT_ERR = 3
	xsec = Xsec()
	xsec.name = "STAR Run-9 500 GeV, parton level"
	for row in xsec_exp:
		xsec.pt_min.append(row[COL_EXP_PT_MIN])
		xsec.pt_max.append(row[COL_EXP_PT_MAX])
		uncertainty_plus = row[COL_EXP_STAT_ERR]
		uncertainty_minus = row[COL_EXP_STAT_ERR]
		xsec.value.append(row[COL_EXP_XSEC])
		xsec.uncertainty_plus.append(uncertainty_plus)
		xsec.uncertainty_minus.append(uncertainty_minus)
	return xsec


def calc_xsec_max_dev(xsecs, xsec_central):
	"""
	Create a new Xsec with error bars produced from highest deviation
	from *xsec_central* of items in *xsecs*.
	"""
	xsec = Xsec()
	xsec.name = xsec_central.name + ", scale uncertainty error bars"
	xsec.pt_max = xsec_central.pt_max
	xsec.pt_min = xsec_central.pt_min
	for i in range(xsec_central.bin_count):
		bin_xsec_values = map(lambda s: get_xsec_value(s, i), xsecs)
		central_value = get_xsec_value(xsec_central, i)
		xsec.value.append(central_value)
		xsec.uncertainty_plus.append(max(bin_xsec_values) -  central_value)
		xsec.uncertainty_minus.append(central_value - min(bin_xsec_values))
	return xsec


def mk_scale_uncertainty(xsecs):
	"""
	Create a new Xsec with error bars corresponding to scale variation uncertainty.
	Central values are copied from Xsec with mur=1,mur=1.
	"""
	scales = map(lambda xsec: (xsec.xmur, xsec.xmuf), xsec_scalevar)
	assert len(set(scales)) == FASTNLO_SCALES_COUNT
	xsecs_scale = dict(zip(scales, xsec_scalevar))
	xsec_central = xsecs_scale[(1.0, 1.0)]
	return calc_xsec_std_dev(xsecs, xsec_central)


def calc_deviation(xs):
        return np.sqrt(np.mean(np.array(xs)**2))


def calc_xsec_std_dev(xsecs, xsec_central):
	"""
	Create a new Xsec with error bars produced from 1 sigma deviation
	from *xsec_central* of items in *xsecs*.
	"""
	xsec = Xsec()
	xsec.name = xsec_central.name + ", replicas 1 sigma error bars"
	xsec.pt_max = xsec_central.pt_max
	xsec.pt_min = xsec_central.pt_min
	for i in range(xsec_central.bin_count):
		central_value = get_xsec_value(xsec_central, i)
		bin_xsec_values = map(lambda s: get_xsec_value(s, i), xsecs)
		bin_xsec_values_plus = np.array(filter(lambda x: x > central_value, bin_xsec_values))
		bin_xsec_values_minus = np.array(filter(lambda x: x < central_value, bin_xsec_values))
		xsec.value.append(central_value)
		xsec.uncertainty_plus.append(calc_deviation(bin_xsec_values_plus - central_value))
		xsec.uncertainty_minus.append(calc_deviation(bin_xsec_values_minus - central_value))
	return xsec


def mk_pdf_variation_uncertainty(xsecs):
	"""
	Create a new Xsec with error bars corresponding to PDF variation uncertainty.
	Central values are copied from Xsec with member=0.
	"""
	members = map(lambda xsec: xsec.member, xsecs)
	xsec_member = dict(zip(members, xsecs))
	xsec_central = xsec_member[0]
	xsecs_without_central = filter(lambda xsec: xsec.member != 0, xsecs)
	return calc_xsec_std_dev(xsecs_without_central, xsec_central)


if __name__ == '__main__':
	ROOT.gROOT.SetBatch(True)
	ROOT.gStyle.SetPalette(54)

	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('--pdf_name', type=str)
	parser.add_argument('--num_replicas', type=int)
	parser.add_argument('--scenario', type=str, default='star2006_200', choices=['star2006_200', 'star2009_500'])
	args = parser.parse_args()

	fp = open("%s_%s" % (args.scenario, args.pdf_name), "r")
	it = read_cppread_output(fp)
	xsec_scalevar = take(FASTNLO_SCALES_COUNT, it)
	xsec_all_members = take(args.num_replicas + 1, it)
	xsec_replicas = filter(lambda xsec: xsec.member != 0, xsec_all_members)
	fp.close()

	xsec_any = xsec_replicas[0]
	pt_bins = xsec_any.pt_bins
	if args.scenario == 'star2006_200':
		XSEC_MIN_LOG = -4
		XSEC_MAX_LOG = +5.5
		SQRT_S = 200
	elif args.scenario == 'star2009_500':
		XSEC_MIN_LOG = 0
		XSEC_MAX_LOG = +8
		SQRT_S = 500
	XSEC_BINS = 500
	xsec_bins = np.logspace(XSEC_MIN_LOG, XSEC_MAX_LOG, XSEC_BINS)

	c = ROOT.TCanvas("c", "", 500, 600)
	MARGIN = 0.005
	PAD_MIDDLE = 0.25
	p_upper = ROOT.TPad("p_upper", "", MARGIN, PAD_MIDDLE, 1 - MARGIN, 1 - MARGIN)
	p_upper.Draw()
	p_lower = ROOT.TPad("p_lower", "", MARGIN, MARGIN, 1 - MARGIN, PAD_MIDDLE)
	p_lower.Draw()
	p_upper.cd()
	p_upper.SetLogy()
	legend = ROOT.TLegend(0.12, 0.12, 0.4, 0.22)
	legend.SetTextSize(0.03)
	legend.SetLineColor(ROOT.kWhite)
	title = "Inclusive jet cross section distribution of %i %s replicas, #sqrt{s} = %i GeV" \
		% (args.num_replicas, args.pdf_name, SQRT_S)
	h_main = mk_xsec_hist("h_main", title, pt_bins, xsec_bins, main=True)
	h_main.Draw()
	obj = mk_xsec_distribution_plot(xsec_bins, xsec_replicas)
	if args.scenario == 'star2006_200':
		exp_xsec = star_2006_inclusive_xsec()
		exp_plot = mk_xsec_plot(exp_xsec)
		legend.AddEntry(exp_plot, exp_xsec.name, "LEP")
	elif args.scenario == 'star2009_500':
		exp_xsec = star_2009_500_inclusive_xsec()
		# Don't plot xsec yet
	pdf_variation_uncertainty_xsec = mk_pdf_variation_uncertainty(xsec_all_members)
	pdf_variation_uncertainty_plot = mk_xsec_plot(pdf_variation_uncertainty_xsec)
	pdf_variation_uncertainty_plot.SetLineColor(ROOT.kMagenta)
	scale_uncertainty_xsec = mk_scale_uncertainty(xsec_scalevar)
	#scale_uncertainty_plot = mk_xsec_plot(scale_uncertainty_xsec)
	#scale_uncertainty_plot.SetLineColor(ROOT.kGreen)
	legend.AddEntry(pdf_variation_uncertainty_plot, pdf_variation_uncertainty_xsec.name, "LEP")
	legend.Draw()
	p_lower.cd()
	kfactor_plot = mk_xsec_exp_kfactor_plot(pdf_variation_uncertainty_xsec, scale_uncertainty_xsec, exp_xsec)
	c.SaveAs("xsec_replicas_%s_%s.pdf" % (args.scenario, args.pdf_name))
	c = ROOT.TCanvas()
	c.Divide(4, 4)
	bbbd_objs = mk_xsec_bin_by_bin_distribution_plots(c, args.pdf_name, xsec_replicas)
	bbb_objs = mk_xsec_bin_by_bin_plots(c, pdf_variation_uncertainty_xsec)
	c.SaveAs("xsec_replicas_%s_%s_distribution.pdf" % (args.scenario, args.pdf_name))
