#!/usr/bin/env python

import os
import sys
import array
from math import sqrt, erf
import numpy as np

sys.path.append(os.path.join(os.path.dirname(__file__), "lib/python2.7/site-packages"))

import lhapdf
import ROOT

PDG_GLUON = 21

MAX_MEMBERS = 100

STD_CL = erf(1/sqrt(2)) * 100

def calc_pdf(pdf, Q, xs):
	return np.array(map(lambda x: pdf.xfxQ(PDG_GLUON, x, Q), xs))

def mk_array(ar):
	if isinstance(ar, np.ndarray):
		ar = list(ar)
	return array.array("d", ar)

def calc_deviation(xs):
	return np.sqrt(np.mean(np.array(xs)**2))

def cl_to_std_devs(confidence_level):
	if confidence_level == STD_CL:
		return 1
	from scipy.special import erfinv
	from math import sqrt
	return erfinv(confidence_level / 100.0) * sqrt(2)

def draw_pdf(Q, xs, central_set, replica_set, relative_set, confidence_level, color, show_replicas, invert_line_styles, legend, gs, g_outlines):
	xs = filter(lambda x: x < 1.0, xs)
	pdf = lhapdf.mkPDF(central_set, 0)
	central_ys = calc_pdf(pdf, Q, xs)
	if relative_set:
		pdf = lhapdf.mkPDF(relative_set, 0)
		relative_ys = calc_pdf(pdf, Q, xs)
	dys_all = {}
	for member in range(1, MAX_MEMBERS+1):
		pdf = lhapdf.mkPDF(replica_set, member)
		ys = calc_pdf(pdf, Q, xs)
		dys = ys - central_ys
		if relative_set:
			ys /= relative_ys
			dys /= relative_ys
		if show_replicas:
			g = ROOT.TGraph(len(xs), mk_array(xs), mk_array(ys))
			g.SetLineColor(color+4)
			g.SetLineColor(ROOT.TColor.GetColorTransparent(color+1, 0.1))
			g.Draw("SAME")
			gs.append(g)
		for i, dy in enumerate(dys):
			if i not in dys_all:
				dys_all[i] = []
			dys_all[i].append(dy)
	ys_std_p = []
	ys_std_n = []
	for i, y in enumerate(central_ys):
		var_p = calc_deviation(filter(lambda dy: (not np.isnan(dy)) and dy>=0, dys_all[i]))
		ys_std_p.append(var_p)
		var_n = calc_deviation(filter(lambda dy: (not np.isnan(dy)) and dy<=0, dys_all[i]))
		ys_std_n.append(var_n)
	central_ys_plot = central_ys
	if relative_set:
		central_ys_plot /= relative_ys
	std_devs = cl_to_std_devs(confidence_level)
	ys_h = np.array(ys_std_p) * std_devs
	ys_l = np.array(ys_std_n) * std_devs
	central_color = ROOT.TColor.GetColorTransparent(color, 0.9)
	uncertainty_bars_color = ROOT.TColor.GetColorTransparent(color-4, 0.9)
	g_h = ROOT.TGraph(len(xs), mk_array(xs), mk_array(central_ys_plot + ys_h))
	g_h.SetLineColor(uncertainty_bars_color)
	g_h.SetLineWidth(2)
	g_outlines.append(g_h)
	g_l = ROOT.TGraph(len(xs), mk_array(xs), mk_array(central_ys_plot - ys_l))
	g_l.SetLineColor(uncertainty_bars_color)
	g_l.SetLineWidth(2)
	g_outlines.append(g_l)
	g_central = ROOT.TGraph(len(xs), mk_array(xs), mk_array(central_ys_plot))
	g_central.SetLineColor(central_color)
	g_central.SetLineWidth(2)
	if invert_line_styles:
		g_central.SetLineStyle(2)
	else:
		g_l.SetLineStyle(2)
		g_h.SetLineStyle(2)
	g_outlines.append(g_central)
	legend.AddEntry(g_central, central_set, "L")

def mk_plot(pdf_sets, Q, xs, relative, confidence_level, output_filename, show_replicas=True, invert_line_styles=False):
	c = ROOT.TCanvas()
	c.SetLogx()
	if show_replicas:
		title_fmt = "PDF central curves and replicas with %.0f%% c.l. bands"
	else:
		title_fmt = "PDF central curves and replicas spread %.0f%% c.l. bands"
	h = ROOT.TH1F("h", title_fmt % confidence_level, 1, min(xs), max(xs))
	h.SetStats(False)
	h.SetXTitle("x")
	ytitle = "xg (x, Q = %.1f GeV)" % Q
	if relative:
		ytitle += " relative to %s" % relative
	h.SetYTitle(ytitle)
	y_range = (0.5, 1.6) if relative else (0, 40)
	h.GetYaxis().SetRangeUser(*y_range)
	h.Draw()
	gs = []
	g_outlines = []
	legend = ROOT.TLegend(0.25, 0.7, 0.75, 0.88)
	legend.SetTextSize(0.05)
	for central_set, replica_set, color in pdf_sets:
		draw_pdf(Q, xs, central_set, replica_set, relative, confidence_level, color, show_replicas, invert_line_styles, legend, gs, g_outlines)
	for g in g_outlines:
		g.Draw("SAME")
	legend.Draw()
	c.SaveAs(output_filename)

if __name__ == '__main__':
	ROOT.gROOT.SetBatch(True)
	import argparse
	parser = argparse.ArgumentParser(description="Make PDF plots")
	parser.add_argument('--Q', type=int, default=10, help="Q of the PDF in GeV")
	parser.add_argument('--logx_min', type=int, default=-4, help="x axis minimal logarithm (default: -4)")
	parser.add_argument('--confidence_level', type=float, default=STD_CL, help="(default: 68%%)")
	parser.add_argument('--scenario', default='default', choices=['default', 'nnpdf_benchmark', 'cl_check'])
	args = parser.parse_args()

	xs = np.logspace(args.logx_min, 0, 1000)
	cl = args.confidence_level

	if args.scenario == 'default':
		pdf_sets = [
			("CT10nlo", "CT10nlo_rand17156", ROOT.kRed),
			("HERAPDF15NLO_EIG", "HERAPDF15NLO_EIG_rand5560", ROOT.kBlue),
			("NNPDF30_nlo_as_0118", "NNPDF30_nlo_as_0118", ROOT.kGreen)
		]
		mk_plot(pdf_sets, args.Q, xs, False, cl, "xg_Q%i_cl%.0f.pdf" % (args.Q, cl))
		mk_plot(pdf_sets, args.Q, xs, "NNPDF30_nlo_as_0118", cl, "xg_Q%i_cl%.0f_relative.pdf" % (args.Q, cl))
		for pdf_set in pdf_sets:
			mk_plot([pdf_set], args.Q, xs, pdf_set[0], cl, "xg_Q%i_cl%.0f_relative_%s.pdf" % (args.Q, cl, pdf_set[0]))
	elif args.scenario == 'nnpdf_benchmark':
		pdf_sets = [
			("NNPDF23_nnlo_as_0118", "NNPDF23_nnlo_as_0118", ROOT.kGreen),
			("CT10nnlo", "CT10nnlo_rand9722", ROOT.kRed),
			("MSTW2008nnlo90cl", "MSTW2008nnlo90cl_rand30038", ROOT.kBlue)
		]
		mk_plot(pdf_sets, args.Q, xs, "NNPDF23_nnlo_as_0118", cl, "xg_nnpdf_benchmark_Q%i_cl%.0f_1.pdf" % (args.Q, cl), show_replicas=False, invert_line_styles=True)
		pdf_sets = [
			("NNPDF23_nnlo_as_0118", "NNPDF23_nnlo_as_0118", ROOT.kGreen),
			("abm11_5n_nnlo", "abm11_5n_nnlo_rand13165", ROOT.kMagenta),
			("HERAPDF15NNLO_EIG", "HERAPDF15NNLO_EIG_rand3923", ROOT.kOrange)
		]
		mk_plot(pdf_sets, args.Q, xs, "NNPDF23_nnlo_as_0118", cl, "xg_nnpdf_benchmark_Q%i_cl%.0f_2.pdf" % (args.Q, cl), show_replicas=False, invert_line_styles=True)
	elif args.scenario == 'cl_check':
		# This scenario compares how hessian2replicas deals with the same set
		# equipped with error vectors of different c.l. The end plots should be the same.
		pdf_sets = [
			("MSTW2008nnlo90cl", "MSTW2008nnlo90cl_rand30038", ROOT.kBlue),
			("MSTW2008nnlo68cl", "MSTW2008nnlo68cl_rand26206", ROOT.kGreen)
		]
		mk_plot(pdf_sets, args.Q, xs, "MSTW2008nnlo90cl", cl, "xg_cl_check_Q%i_cl%.0f.pdf" % (args.Q, cl))
