#!/usr/bin/env python3

import argparse
import random
import math
import os

WORKDIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), "workdir")

parser = argparse.ArgumentParser()
parser.add_argument("--events_per_job", type=str, default="100M",
                    help="total event count to produce")
parser.add_argument("--job_name", type=str, default="unnamed",
                    help="name of the job (acts as a filename prefix as well)")
parser.add_argument("--num_jobs", type=int, default=1000,
                    help="count of jobs for each calculation order (LO, NLO)")
parser.add_argument("--seed", type=int, default=None,
                    help="random seed")
parser.add_argument("--slurm_cpus_per_task", type=int, default=1,
                    help="number of threads per node")
parser.add_argument("--prefix", type=str, default=WORKDIR,
                    help="prefix path for nlojet++/fastnlo_interface installation")
args = parser.parse_args()

if args.events_per_job[-1] == "G":
	# billions
	args.events_per_job = int(args.events_per_job[:-1]) * 1000000000
elif args.events_per_job[-1] == "M":
	# millions
	args.events_per_job = int(args.events_per_job[:-1]) * 1000000
else:
	args.events_per_job = int(args.events_per_job)

if args.seed is not None:
	random.seed(args.seed)

print(f"""\
#!/bin/bash
  
#SBATCH -J {args.job_name}
#SBATCH -p general
#SBATCH -o output_%j.txt
#SBATCH -e output_%j.err
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task={args.slurm_cpus_per_task}
#SBATCH --time=4-00:00:00

sh -c 'while true; do pstree '$$'; sleep 300; done' &
mkdir -p log time

(
""")

num_job_digits = math.ceil(math.log10(max(2, args.num_jobs - 1)))
for i in range(args.num_jobs):
    for approx in ["born", "nlo"]:
       task_name = f"{args.job_name}_{i:0{num_job_digits}}"
       seed = random.randint(0, 4294967296)
       print(f"printf \"command time -vo time/{task_name} {args.prefix}/bin/nlojet++ --calculate -c{approx} -n{task_name} -s{seed} -u {args.prefix}/lib/fastnlo_interface_nlojet/libInclusiveNJets.la --max-event {args.events_per_job} 2>&1 > log/{task_name}_{approx}.log\\0\"")

print(f"""
) | xargs --max-procs={args.slurm_cpus_per_task} --no-run-if-empty -0 -n1 sh -c
""")
# --delimiter="\\r"
